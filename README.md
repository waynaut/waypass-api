# Waypass Sample Projects #

This repo contains a sample project demostrating the use of Waypass API to perform multimodal booking.

For further informations and API guide refer to this [repo wiki](../wiki)

### Setup ###

* Prerequisites: Java 1.8+, Maven 3.3+
* Valid credentials necessary: contact [waynaut.com](http://waynaut.com) to get yours

```
git clone https://bitbucket.org/waynaut/waypass-api.git
cd waypass-api
mvn clean install

```