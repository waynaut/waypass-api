package com.waynaut.waypass.client.utils;
/*
 * DownloadTicketIT.java
 *
 *
 * Copyright(c) 2016 Youmove.me Srl -  All Rights Reserved.
 * This software is the proprietary information of Youmove.me
 *
 */

import com.squareup.okhttp.*;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;
import com.waynaut.waypass.client.ApiClient;
import com.waynaut.waypass.client.ApiException;
import com.waynaut.waypass.client.api.OrderresourceApi;
import com.waynaut.waypass.client.model.Order;
import com.waynaut.waypass.client.model.OrderItem;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.concurrent.TimeUnit;


public class DownloadTicketIT {
    private final static ApiClient apiClient = new ApiClient();
    private final OrderresourceApi orderresourceApi = new OrderresourceApi(apiClient);

    @BeforeClass
    public static void init() throws Exception {
        String baseUrl = "";
        String clientId = "";
        String password = "";
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(baseUrl + "/api/auth/login")
                .post(RequestBody.create(MediaType.parse("application/json"), "{\"username\":\"" + clientId + "\", \"password\": \"" + password + "\"}"))
                .build();
        Response response = client.newCall(request).execute();

        try (ResponseBody responseBody = response.body()) {
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code " + response);
            }
        }

        String authorizationHeader = response.headers().get("Authorization");
        String bearer = Optional.ofNullable(authorizationHeader)
                .map(s -> s.replace("Bearer ", ""))
                .orElseThrow(() -> new IllegalArgumentException("Login failed!"));

        apiClient.getHttpClient().interceptors().add(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        apiClient.setBasePath(baseUrl);
        apiClient.getHttpClient().setConnectTimeout(300, TimeUnit.SECONDS);
        apiClient.getHttpClient().setReadTimeout(300, TimeUnit.SECONDS);
        apiClient.addDefaultHeader("Authorization", "Bearer " + bearer);
    }

    @Test
    public void download() throws ApiException, IOException {
        String path = "";       //path where to save
        String orderId = "";

        Order order = orderresourceApi.getOrderByIdUsingGET(orderId);

        if(("BOOKED").equals(order.getStatus()) && ("PAID").equals(order.getPaymentStatus()) && ("FULFILLMENT_COMPLETE").equals(order.getFulfillmentStatus())){

            for(String orderItemId : order.getOrderItems().keySet()){
                OrderItem orderItem = order.getOrderItems().get(orderItemId);
                for(String index : orderItem.getTicketFiles().keySet()){
                    InputStream inputStream = new FileInputStream(orderresourceApi.getTicketUsingGET(orderId, Integer.parseInt(orderItemId), index));
                    Files.copy(inputStream, Paths.get(path));
                }
            }
        }
    }
}
