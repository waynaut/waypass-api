package com.waynaut.waypass.client.utils;
/*
 * WaypassClientSample.java
 *
 *
 * Copyright(c) 2017 Youmove.me Srl -  All Rights Reserved.
 * This software is the proprietary information of Youmove.me
 *
 */

import com.squareup.okhttp.*;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;
import com.waynaut.waypass.client.ApiClient;
import com.waynaut.waypass.client.api.CartresourceApi;
import com.waynaut.waypass.client.api.OrderresourceApi;
import com.waynaut.waypass.client.model.*;
import com.waynaut.waypass.client.model.Address;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotNull;


public class WaypassClientSample {
    private final static ApiClient apiClient = new ApiClient();
    private final CartresourceApi cartresource = new CartresourceApi(apiClient);
    private final OrderresourceApi orderresourceApi = new OrderresourceApi(apiClient);
    private static final String TRENITALIA_PRODUCTID = "TIR-001";

    @BeforeClass
    public static void init() throws Exception {
        String baseUrl = "";
        String clientId = "";
        String password = "";
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(baseUrl + "/api/auth/login")
                .post(RequestBody.create(MediaType.parse("application/json"), "{\"username\":\"" + clientId + "\", \"password\": \"" + password + "\"}"))
                .build();
        Response response = client.newCall(request).execute();

        try (ResponseBody responseBody = response.body()) {
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code " + response);
            }
        }

        String authorizationHeader = response.headers().get("Authorization");
        String bearer = Optional.ofNullable(authorizationHeader)
                .map(s -> s.replace("Bearer ", ""))
                .orElseThrow(() -> new IllegalArgumentException("Login failed!"));

        apiClient.getHttpClient().interceptors().add(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        apiClient.setBasePath(baseUrl);
        apiClient.getHttpClient().setConnectTimeout(300, TimeUnit.SECONDS);
        apiClient.getHttpClient().setReadTimeout(300, TimeUnit.SECONDS);
        apiClient.addDefaultHeader("Authorization", "Bearer " + bearer);
    }

    @Test
    public void createCartAndBook() throws Exception {
        Integer customerReference = 123456789;

        /*****************/
        /* Cart creation */
        /*****************/
        CartRequest cartRequest = new CartRequest();
        cartRequest.setLocale(Locale.ITALY.toLanguageTag());
        cartRequest.setCurrency("EUR");
        cartRequest.setPassengers(1);
        Cart cart = cartresource.createCartUsingPOST(cartRequest);
        assertNotNull(cart.getId());



        /*****************/
        /* Customer data */
        /*****************/
        Customer customer = new Customer();
        customer.setTitle(Customer.TitleEnum.MR);
        customer.setFirstName("John");
        customer.setLastName("Doe");
        customer.setPhone("+39 0287237067");
        customer.setEmail("john.doe@email.com");
        customer.setDateOfBirth(LocalDate.of(1980, Month.APRIL, 1));
        customer.setVatCode("1234");

        com.waynaut.waypass.client.model.Address address = new Address();
        address.setAddressLine1("Via Monte Nevoso, 10");
        address.setPhone("+39-0287237067");
        address.setCity("Milan");
        address.setPostalCode("20132");
        address.setCountry("IT");
        customer.setInvoiceAddress(address);
        customer.setPrimaryAddress(address);

        cartresource.setCustomerUsingPOST(cart.getId(), String.valueOf(customerReference), customer);


        /******************/
        /* Travelers data */
        /******************/
        Traveler traveler = new Traveler();
        traveler.setTitle(Traveler.TitleEnum.MR);
        traveler.setFirstName("John");
        traveler.setLastName("Doe");
        traveler.setDateOfBirth(LocalDate.of(1980, Month.APRIL, 1));
        traveler.setTravelerType(Traveler.TravelerTypeEnum.ADULT);

        cartresource.putTravelerUsingPOST(cart.getId(), 0, traveler);


        /**************************/
        /* CartItem fares request */
        /**************************/
        SupplierProductIdentifier supplierProductIdentifier = new SupplierProductIdentifier();
        supplierProductIdentifier.getProperties().put("journeySolutionCode", "830001700,830008409,1708151700,549647,0,0,1708150000");

        CartItemFaresRequest cartItemFaresRequest = new CartItemFaresRequest();
        cartItemFaresRequest.setSupplierProductIdentifier(supplierProductIdentifier);
        cartItemFaresRequest.setProductId(TRENITALIA_PRODUCTID);
        cartItemFaresRequest.setCurrency(cart.getCurrency());
        cartItemFaresRequest.setLocale(cart.getLocale());

        CartItemFaresResponse cartItemFaresResponse = cartresource.getFaresUsingPOST(cartItemFaresRequest);

        CartFare cf = cartItemFaresResponse.getRestCartItemFares().get(0);
        FareIdentifier fareIdentifier = new FareIdentifier();
        fareIdentifier.setCategory(cf.getCategory());
        fareIdentifier.setCode(cf.getCode());
        fareIdentifier.setType(FareIdentifier.TypeEnum.TRAVELER_FARE);


        /********************/
        /* CartItem request */
        /********************/
        CartItemRequest cartItemRequest = new CartItemRequest();
        cartItemRequest.setSupplierProductIdentifier(supplierProductIdentifier);
        cartItemRequest.setProductId(TRENITALIA_PRODUCTID);
        cartItemRequest.setFareIdentifiers(Arrays.asList(fareIdentifier));
        cartItemRequest.setDirection(CartItemRequest.DirectionEnum.OUTGOING);

        cartresource.putCartItemUsingPUT(cart.getId(), 1, cartItemRequest);


        /*********************************/
        /* Checkout and  booking request */
        /*********************************/
        Cart checkedOutCart = cartresource.checkoutCartUsingPOST(cart.getId());

        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setId(UUID.randomUUID().toString());
        paymentRequest.setType(PaymentRequest.TypeEnum.DIRECT);
        paymentRequest.setCurrency(checkedOutCart.getCurrency());
        paymentRequest.setAmount(checkedOutCart.getTotalPrice());
        paymentRequest.setNonce(paymentRequest.getId());

        OrderBookingRequest orderBookingRequest = new OrderBookingRequest();
        orderBookingRequest.setCartId(checkedOutCart.getId());
        orderBookingRequest.setOrderReference(UUID.randomUUID().toString());
        orderBookingRequest.setPaymentRequests(Arrays.asList(paymentRequest));

        Order order = orderresourceApi.makeOrderUsingPOST(orderBookingRequest);
        assertNotNull(order.getId());
    }

}