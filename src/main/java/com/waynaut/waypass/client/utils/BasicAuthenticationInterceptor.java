package com.waynaut.waypass.client.utils;
/*
 * BasicAuthenticationInterceptor.java
 *
 *
 * Copyright(c) 2016 Youmove.me Srl -  All Rights Reserved.
 * This software is the proprietary information of Youmove.me
 *
 */

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;
import com.waynaut.waypass.client.ApiClient;

import java.io.IOException;

public class BasicAuthenticationInterceptor implements Interceptor {
    private final ApiClient apiClient;
    private final String userName;
    private final String password;
    private final String loginUrl;
    private final UsernamePasswordAuthenticator usernamePasswordAuthenticator = new UsernamePasswordAuthenticator();

    public BasicAuthenticationInterceptor(ApiClient apiClient, String loginUrl, String userName, String password) {
        this.apiClient = apiClient;
        this.userName = userName;
        this.password = password;
        this.loginUrl = loginUrl;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        final Request request = chain.request();

        Response response = chain.proceed(request);
        if (response.code() == 401 || response.code() == 403) {
            try (ResponseBody responseBody = response.body()) {
                String bearer = usernamePasswordAuthenticator.login(loginUrl, userName, password);
                apiClient.setAccessToken(bearer);
                return chain.proceed(request.newBuilder().addHeader("Authorization", "Bearer " + bearer).build());
            }
        }
        return response;
    }

}