package com.waynaut.waypass.client.utils;/*
 * UsernamePasswordLogin.java
 *
 *
 * Copyright(c) 2016 Youmove.me Srl -  All Rights Reserved.
 * This software is the proprietary information of Youmove.me
 *
 */

import com.squareup.okhttp.*;

import java.io.IOException;
import java.util.Optional;

public class UsernamePasswordAuthenticator {
    public String login(String loginUrl, String username, String password) throws IOException {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(loginUrl)
                .post(RequestBody.create(MediaType.parse("application/json"), "{\"username\":\"" + username + "\", \"password\": \"" + password + "\"}"))
                .build();
        Response response = client.newCall(request).execute();

        try (ResponseBody responseBody = response.body()) {
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code " + response);
            }
        }

        String authorizationHeader = response.headers().get("Authorization");
        return Optional.ofNullable(authorizationHeader)
                .map(s -> s.replace("Bearer ", ""))
                .orElseThrow(() -> new IllegalArgumentException("Login failed!"));
    }
}
